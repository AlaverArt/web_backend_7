<html>
  <head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
      <?php echo file_get_contents("style.css"); ?>
    </style>
  </head>
  <body>

<header class="shapka">
	<div class="logo_name">
		<h1>Свяжитесь с нами</h1>	
	</div>
</header>

<?php
if(!empty($_COOKIE['fio_error'])||!empty($_COOKIE['e_mail_error'])||!empty($_COOKIE['year_error'])||!empty($_COOKIE['biography_error'])){
  print '<div style="text-align:center;"><a style="color:red;margin:0 auto;">*</a><a>Это обязательные поля</a></div>';
}
else{
  if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
      print($message);
    }
    print('</div>');
  }
}
// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

<div class="spisok_table_form_back">
<div class="spisok_table_form_back2">
  <form  action="index.php" method="POST">
    <h2><strong>Форма</strong></h2>
    <label>
					Ваше имя:<br />
					<input name="name1" <?php if ($errors['fio']) {print 'class="error"';} ?>  type="text"
					value="<?php print ($values['fio']); ?>" />
			</label><?php if ($errors['fio']) {print '<a class="error_msg">*</a>';} ?><br />
        
      <label>
          Ваш email:<br />
          <input <?php if ($errors['e_mail']) {print 'class="error"';} ?> name="e_mail"
          value="<?php print $values['e_mail']; ?>"
          type="email" />
      </label><?php if ($errors['e_mail']) {print '<a class="error_msg">*</a>';} ?><br />
      
      <label>
          Дата рождения:<br />
          <input <?php if ($errors['year']) {print 'class="error"';} ?> name="year"
          value="<?php print $values['year']; ?>"
          type="date" />
      </label><?php if ($errors['year']) {print '<a class="error_msg">*</a>';} ?><br />
      
      Ваш пол:<br />
      <label><input type="radio" checked="checked"
        name="gender" value="m" />
        Муж</label>
      <label><input type="radio"
        name="gender" value="f" />
        Жен</label><br />
        
      Количество конечностей:<br />
      <label><input type="radio" checked="checked"
        name="limbs" value="0" />
        0</label>
      <label><input type="radio" checked="checked"
        name="limbs" value="1" />
        1</label>
      <label><input type="radio" checked="checked"
        name="limbs" value="2" />
        2</label>
      <label><input type="radio" checked="checked"
        name="limbs" value="3" />
        3</label>
      <label><input type="radio"
        name="limbs" value="4" />
        4</label><br />
        
      <label>
        Сверхспособности:
        <br />
        <select name="super_skill" value="<?php print $values['super_skill']; ?>">
        <option name="ability_god" type="text" value="ability_god">Бессмертие</option>
        <option name="ability_fly" type="text" value="ability_fly">Левитация</option>
        <option name="ability_fireball" type="text" value="ability_fireball">Огненные шары</option>
        </select>
      </label><br />
      
      <label>
        Биография:<br />
        <textarea <?php if ($errors['biography']) {print 'class="error"';} ?> name="biography"><?php print $values['biography']; ?></textarea>
      </label><?php if ($errors['biography']) {print '<a class="error_msg">*</a>';} ?><br />

      <br />
      <label><input type="checkbox" checked="checked"
      name="check-1" />
      С контрактом ознакомлен</label><br />
      
      <input type="submit" value="Отправить" />
  </form>

  <a href="login.php">Войти</a>
</div>
</div>

    <footer class="footer">
	    <p>(c)Alaverdyan 2021</p>
    </footer>
  </body>
</html>