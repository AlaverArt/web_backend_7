<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

function debug_to_console( $data ) {
  $output = $data;
  if ( is_array( $output ) )
      $output = implode( ',', $output);

  echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

function generate($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0');
// Генерируем пароль
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
// Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['e_mail'] = !empty($_COOKIE['e_mail_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    //$messages[] = '<div class="error">Заполните имя.</div>';//не пользуюсь
  }
  if($errors['e_mail']){
    setcookie('e_mail_error', '', 100000);
  }
  if($errors['year']){
    setcookie('year_error', '', 100000);
  }
  if($errors['biography']){
    setcookie('biography_error', '', 100000);
  }
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  //$values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['e_mail'] = empty($_COOKIE['e_mail_value']) ? '' : $_COOKIE['e_mail_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['super_skill'] = empty($_COOKIE['super_skill_value']) ? '' : $_COOKIE['super_skill_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
  // TODO: аналогично все поля.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД

    $user = 'u21048';
    $password = '5786899';
    $db = new PDO('mysql:host=localhost;dbname=u21048', $user, $password);

    $get_sql = "SELECT name1,e_mail,year,gender,biography FROM form_inf WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')";
    $get_e_mail_sql = "SELECT e_mail FROM form_inf WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')";
    $get_year_sql = "SELECT year FROM form_inf WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')";
    $get_gender_sql = "SELECT gender FROM form_inf WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')"; 
    $get_limbs_sql = "SELECT limbs FROM form_inf WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')";
    $get_biography_sql = "SELECT biography FROM form_inf WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')";
    
    //$values = array();
    
    $temp = array();

    $db->exec("set names utf8");
    $stmt = $db->prepare($get_sql);
    $stmt->execute();
    $temps = $stmt->fetch(PDO::FETCH_ASSOC);
    $i = 1;
    foreach($temps as $temp){
      if($i==1 && $temp != '')
      {
        $values['fio'] = $temp;
      }
      else if($i==2)
      {
        $values['e_mail'] = $temp;
      }
      else if($i==3)
      {
        $values['year'] = $temp;
      }
      else if($i==4)
      {
        $values['gender'] = $temp;
      }
      if($i==5)
      {
        $values['biography'] = $temp;
      }
      $i++;
      debug_to_console( $temp );
      debug_to_console( "," );
    }
  
    // и заполнить переменную $values,
    // предварительно санитизовав.
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
  setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  setcookie('super_skill_value', $_POST['super_skill'], time() + 30 * 24 * 60 * 60);

  if (empty($_POST['name1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', htmlspecialchars($_POST['name1']), time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['e_mail'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('e_mail_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('e_mail_value', htmlspecialchars($_POST['e_mail']), time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['year'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', htmlspecialchars($_POST['biography']), time() + 30 * 24 * 60 * 60);
  }



  // *************
  // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
  // Сохранить в Cookie признаки ошибок и значения полей.
  // *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('e_mail_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('biography_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }

  $name1 = htmlspecialchars($_POST['name1']);
  $e_mail = htmlspecialchars($_POST['e_mail']);
  $year = $_POST['year'];
  $gender = $_POST['gender'];
  $limbs = $_POST['limbs'];
  $biography = htmlspecialchars($_POST['biography']);
  $ability = $_POST['super_skill'];

  $user = 'u21048';
  $password = '5786899';
  $db = new PDO('mysql:host=localhost;dbname=u21048', $user, $password);

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    $update_sql = "UPDATE form_inf SET name1 = ':name1', 
                                       e_mail = ':e_mail',
                                       year = ':year',
                                       gender = ':gender',
                                       limbs = ':limbs',
                                       biography = ':biography'
                                       WHERE id = (SELECT id FROM users WHERE login = '{$_SESSION['login']}')";
    // кроме логина и пароля.
    try{
      $db->exec("set names utf8");
      $stmt = $db->prepare($update_sql);
      $stmt = $db->bindParam(':name1', $name1);
      $stmt = $db->bindParam(':e_mail', $e_mail);
      $stmt = $db->bindParam(':year', $year);
      $stmt = $db->bindParam(':gender', $gender);
      $stmt = $db->bindParam(':limbs', $limbs);
      $stmt = $db->bindParam(':biography', $biography);
      $stmt->execute();
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().

    $login = generate(rand(1,25));

    $password = generate(rand(1,25));

    $hash_pass=password_hash($password, PASSWORD_DEFAULT);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $password);

    // Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    try {
      $db->exec("set names utf8");
      $data1 = array('name1' => $name1, 'e_mail'=> $e_mail, 'year'=>$year, 'gender'=>$gender, 'limbs'=>$limbs, 'biography'=>$biography);
      $data2 = array('ability'=>$ability);
      $q1 = $db->prepare("INSERT INTO form_inf (name1, e_mail, year, gender, limbs, biography) VALUES(:name1, :e_mail, :year, :gender, :limbs, :biography)");
      $q1->execute($data1);
      $q2 = $db->prepare("INSERT INTO form_abil (ability) VALUES(:ability)");
      $q2->execute($data2);

      $stmt = $db->prepare("INSERT INTO users (login, pass) VALUES (:login,:hash_pass)");
      $stmt->bindParam(':login', $login);
      $stmt->bindParam(':hash_pass', $hash_pass);
      $stmt->execute();
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }

  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
